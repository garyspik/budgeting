#base page describes common page actions

class BasePage
 
 attr_reader :drive

 def initialize (driver)
 @driver = driver	
 end

 def visit(url='/')
 	driver.get(ENV['base_url']+url)
 end

 def click(element)
 	driver.find_element(element).click 
 end

 def fill_up(element,input)
 	driver.find_element(element).send_keys(input)
end
end