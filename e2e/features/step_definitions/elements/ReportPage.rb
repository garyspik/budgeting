require_relative './BasePage'

#Actions for the reports page
class ReportPage < BasePage

attr_reader :driver

IN_OUT_GRAPHIC_ELEMENT = {css: "div > svg"}
CATEGORIES_GRAPHIC_ELEMENT = {css: "text"}

def initialize(driver)
    super
end

def in_out_grafic_elements_verification
	driver.find_element(IN_OUT_GRAPHIC_ELEMENT).displayed?
end

def spendinc_by_category_elements_verification
	driver.find_element(CATEGORIES_GRAPHIC_ELEMENT).displayed?
end

end