require_relative './BasePage'

# Actions for the main page
class MainPage < BasePage

 DROP_DOWN = { name: "categoryId" }
 DESCRIPTION_FIELD = { name: "description" }
 VALUE_FIELD = { name: "value" }
 ADD = { xpath: "//button[contains(.,'Add')]" }
 ROWS = {xpath: "//td/div"}
 TOTAL_INFLOW = {css: "section > div > div > div:nth-child(1) > div > div:nth-child(1)"}
 TOTAL_OUTFLOW = {css: "section > div > div > div:nth-child(3) > div > div"}
 WORKING_BALANCE = {css: "section > div > div > div:nth-child(1) > div > div:nth-child(1)"}

 attr_reader :driver

  def initialize(driver)
    super
  end


 def select_category(value)
	dropdown_list = driver.find_element(DROP_DOWN)
	#Get all the options from the dropdown
	options = dropdown_list.find_elements(tag_name: 'option')
	options.each { |option| option.click if option.text ==  value}
end

def add_record_with_parameters(category, description, value)
	select_category(category)
	fill_up(DESCRIPTION_FIELD,description)
	fill_up(VALUE_FIELD,value)
	click(ADD)
end

def is_text_present_in_table (text)
	# Raise an error msg if element is not present on page
	rows = driver.find_elements(ROWS)
	if rows.select {|el| el.text == text}.first == nil
		raise "Following text is absent: " + text
		end
end


def compare_balance
	# Retrive and check if balace calculates correctly
	income = driver.find_element(TOTAL_INFLOW).text 
	outcome = driver.find_element(TOTAL_OUTFLOW).text
	income = income[1..-4].sub(',','.').to_f 
	outcome =outcome[1..-4].sub(',','.').to_f
	working_balance= income - outcome
	verification = working_balance + outcome
if verification != (working_balance +outcome)
	raise "Incorect calculation ERROR"
end
end
end
