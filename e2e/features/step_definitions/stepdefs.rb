require "selenium-webdriver"
require_relative './elements/BasePage.rb'
require_relative './elements/MainPage.rb'
require_relative './elements/ReportPage.rb'

# Define steps described in feauture file relative to each page

# Base url
ENV['base_url'] ='http://localhost:8000'
driver = Selenium::WebDriver.for :chrome
driver.manage.timeouts.implicit_wait = 4
main_page = MainPage.new(driver)
report_page = ReportPage.new(driver)


# Steps definitions
Given("Im on {string}") do |string|
	main_page.visit(string)
end

When("I add record for category {string} with description {string} and value {string}") do |category, description, value|
	main_page.add_record_with_parameters(category, description, value)
end

Then("I should see {string} in table") do |text|
  main_page.is_text_present_in_table(text)
end

Then("I check that balance calculates correctly") do
  main_page.compare_balance
end

When("I check in and out graphic for all elements to be present") do
  report_page.in_out_grafic_elements_verification
end

Then("I check by category graphic for all elements to be present") do
  report_page.spendinc_by_category_elements_verification
end


