Feature: budget app
  tests related to main functionality
  hooks availble '@skip'

    Scenario: Balance calculates correctly
    Given Im on "/"
    When I add record for category "School" with description "Description" and value "100"
    And I add record for category "Income" with description "Description" and value "200"
    Then I check that balance calculates correctly

    Scenario: New record can be added
    Given Im on "/"
    When I add record for category "School" with description "test description" and value "777"
    Then I should see "test description" in table
    And I should see "-$777.00" in table

    Scenario:App can be opened on a report tab
 #This test will  fail due to incorect locator
    Given Im on "/reports/inflow-outflow"
    When I check in and out graphic for all elements to be present
    And Im on "/reports/spending"
    Then I check by category graphic for all elements to be present