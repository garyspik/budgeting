Install bundler and 'selenium-webdriver' gem

gem install bundler

bundle install

Install drivers

Mac:

brew install chromedriver for chrome

brew install geckodriver for firefox

To run tests:

cd to e2e folder and

run with 'cucumber' comand

Test plan

https://docs.google.com/document/d/1uZ7BllQDVsGbsdD0ibTL3N6J1vq-oWHHcCT6bKYve50/edit?usp=sharing

Test implemented:


Test: New record can be added 

Steps:

1. Open app with a link provided ( use http://localhost:8000)for local installation

2.Select category

3.Add description

4.Add value amount

5.Press add button

6.Check the table 

Expected behavior :

Appropriate entry added to the table , description is correct, amount is correct, amount color is green and positive for “income” category, for all other categories - red and negative 


Test: Balance calculates correctly

Steps:

1. Open app with a link provided ( use http://localhost:8000)for local installation

2.Add outflow record

3.Add inflow record

4.Check inflow/outflow/balance calculations on the bottom of the page

Expected behavior :

Total inflow balance calculates correctly, total outflow balance calculates correctly/ Working Balance
is correct


!!This tests will fail

 Test: Report page can be opened
 
Steps:

1. Open app with a link provided ( use http://localhost:8000/reports/inflow-outflow for local installation)

2. Press reports page

Expected behavior:

Report page opens, 2 types of report are available “inflow vs outflow” and  “Spending by category”



